//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlowFootBall
{
    using System;
    using System.Collections.Generic;
    
    public partial class PREDICTION
    {
        public int PredictionId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> MatchId { get; set; }
        public Nullable<int> PredictedTeamId { get; set; }
        public Nullable<int> HomeTeamId { get; set; }
        public Nullable<int> AwayTeamId { get; set; }
        public Nullable<System.DateTime> PredictedDate { get; set; }
    
        public virtual MATCH MATCH { get; set; }
        public virtual USER USER { get; set; }
    }
}
