//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlowFootBall
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEAM_PLAYERS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TEAM_PLAYERS()
        {
            this.PlayerStatus = "TRUE";
        }
    
        public int TeamPlayerId { get; set; }
        public Nullable<int> TeamId { get; set; }
        public string PlayerName { get; set; }
        public string PlayerImage { get; set; }
        public string PlayerStatus { get; set; }
    
        public virtual TEAM TEAM { get; set; }
    }
}
