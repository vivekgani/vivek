﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlowFootBall.App_Code;

namespace FlowFootBall
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FootBall_Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FootBall_Service.svc or FootBall_Service.svc.cs at the Solution Explorer and start debugging.
    public class FootBall_Service : IFootBall_Service
    {
        /***********************Methods for User***********************/
        public void userInsert(UserInput user)
        {
            try
            {
                Users vus = new Users();
                vus.UserId = user.UserId;
                vus.UserName = user.UserName;
                vus.UserImage = user.UserImage;
                vus.MobileNumber = user.MobileNumber;
                vus.EmailId = user.EmailId;
                vus.City = user.City;
                vus.CreatedDate = user.CreatedDate;
                vus.userInsert();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }

        }
        /***********************Methods for CheckingValidRole***********************/
        public bool isValidUser(UserInput user)
        {
            try
            {
                Users vus = new Users();
                vus.UserName = user.UserName;
                vus.MobileNumber = user.MobileNumber;
                return vus.isValidUser();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        /***********************Methods for CheckingRole***********************/
        public bool isValidRole(UserInput user)
        {
            try
            {
                Users vus = new Users();
                vus.UserName = user.UserName;
                vus.MobileNumber = user.MobileNumber;
                return vus.isValidRole();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        /***********************Methods for AdminInsert***********************/
        public void adminInsert(UserInput user)
        {
            try
            {
                Users vus = new Users();
                vus.UserId = user.UserId;
                vus.UserName = user.UserName;
                vus.UserImage = user.UserImage;
                vus.MobileNumber = user.MobileNumber;
                vus.EmailId = user.EmailId;
                vus.City = user.City;
                vus.CreatedDate = user.CreatedDate;
                vus.adminInsert();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        /***********************Methods for teamplayers***********************/

        public List<TeamPlayerInput> adminTeamPlayersSelect()
        {
            try
            {
                TeamPlayers vtp = new TeamPlayers();
                return vtp.adminTeamPlayersSelect();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<TeamPlayerInput> userTeamPlayersSelect()
        {
            try
            {
                TeamPlayers vtp = new TeamPlayers();
                return vtp.userTeamPlayersSelect();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }

        }

        public void teamPlayerInsert(TeamPlayerInput vtplayer)
        {
            try
            {
                TeamPlayers vtp = new TeamPlayers();

            vtp.TeamId = vtplayer.TeamId;
            vtp.PlayerName = vtplayer.PlayerName;
            vtp.PlayerImage = vtplayer.PlayerImage;
            vtp.teamPlayerInsert();
        }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
}

        public void teamPlayerModify(TeamPlayerInput vtplayer)
        {
            try
            {
                TeamPlayers vtp = new TeamPlayers();
            vtp.TeamPlayer_Id = vtplayer.TeamPlayer_Id;
            vtp.TeamId = vtplayer.TeamId;
            vtp.PlayerName = vtplayer.PlayerName;
            vtp.PlayerImage = vtplayer.PlayerImage;
            vtp.teamPlayerModify();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void teamPlayerDelete(TeamPlayerInput vtplayer)
        {
            try
            {
                TeamPlayers vtp = new TeamPlayers();
                vtp.TeamPlayer_Id = vtplayer.TeamPlayer_Id;
                vtp.TeamId = vtplayer.TeamId;
                vtp.PlayerName = vtplayer.PlayerName;
                vtp.PlayerImage = vtplayer.PlayerImage;
                vtp.teamPlayerDelete();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        /***********************Methods for Grounds***********************/
        public List<GroundInput> adminGroundSelect()
        {
            try
            {
                Grounds vg = new Grounds();
                return vg.adminGroundSelect();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        public void groundInsert(GroundInput vground)
        {
            try
            {
                Grounds vg = new Grounds();
                vg.GroundName = vground.GroundName;
                vg.City = vground.City;
                vg.groundInsert();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void groundModify(GroundInput vground)
        {
            try
            {
                Grounds vg = new Grounds();
                vg.GroundId = vground.GroundId;
                vg.GroundName = vground.GroundName;
                vg.City = vground.City;
                vg.groundModify();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void groundDelete(GroundInput vground)
        {
            try
            {
                Grounds vg = new Grounds();
                vg.GroundId = vground.GroundId;
                vg.GroundName = vground.GroundName;
                vg.City = vground.City;
                vg.groundDelete();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

            /***********************Methods for Matches***********************/

            public DataSet userMatchSelect()
        {
            try
            {
                Matches vms = new Matches();
                return vms.userMatchSelect();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<MatchInput> adminMatchSelect()
        {
            try
            {
                Matches vmatches = new Matches();
                return vmatches.adminMatchselect();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        public void matchInsert(MatchInput vmatch)
        {
            try
            {
                Matches vmatches = new Matches();
                vmatches.HomeTeam = vmatch.HomeTeam;
                vmatches.AwayTeam = vmatch.AwayTeam;
                vmatches.WinnerTeamId = vmatch.WinnerTeamId;
                vmatches.GroundId = vmatch.GroundId;
                vmatches.MatchDate = Convert.ToDateTime(vmatch.MatchDate);
                vmatches.SeasonId = vmatch.SeasonId;
                vmatches.matchInsert();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }

        }

        public void matchModify(MatchInput vmatch)
        {
            try
            {
                Matches vmatches = new Matches();
                vmatches.MatchId = vmatch.MatchId;
                vmatches.HomeTeam = vmatch.HomeTeam;
                vmatches.AwayTeam = vmatch.AwayTeam;
                vmatches.WinnerTeamId = vmatch.WinnerTeamId;
                vmatches.GroundId = vmatch.GroundId;
                vmatches.MatchDate = Convert.ToDateTime(vmatch.MatchDate);
                vmatches.SeasonId = vmatch.SeasonId;
                vmatches.matchModify();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void matchDelete(MatchInput vmatch)
        {
            try
            {
                Matches vmatches = new Matches();
                vmatches.MatchId = vmatch.MatchId;
                vmatches.matchDelete();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        /***********************Methods for Sponsors***********************/

        public List<SponsorInput> userSponsorSelect()
        {
            try
            {
                Sponsors vss = new Sponsors();
                return vss.userSponsorSelect();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }


        public List<SponsorInput> adminSponsorSelect()
        {
            try
            {
                Sponsors vss = new Sponsors();
                return vss.adminSponsorSelect();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void sponsorInsert(SponsorInput vsponsor)
        {
            try
            {
                Sponsors vss = new Sponsors();
                vss.SponsorName = vsponsor.SponsorName;
                vss.SponsorImage = vsponsor.SponsorImage;
                vss.SeasonId = vsponsor.SeasonId;
                vss.sponsorInsert();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        public void sponsorModify(SponsorInput vsponsor)
        {
            try
            {
                Sponsors vss = new Sponsors();
                vss.SponsorId = vsponsor.SponsorId;
                vss.SponsorName = vsponsor.SponsorName;
                vss.SponsorImage = vsponsor.SponsorImage;
                vss.SeasonId = vsponsor.SeasonId;
                vss.sponsorModify();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void sponsorDelete(SponsorInput vsponsor)
        {
            try
            {
                Sponsors vss = new Sponsors();

                vss.SponsorId = vsponsor.SponsorId;
                vss.sponsorDelete();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        /*******************************Method for Season***************************/

        public List<SeasonInput> adminSeasonSelect()
        {
            try
            {
                Seasons vseason = new Seasons();
                return vseason.adminSeasonSelect();
            }
            catch (Exception exp)
            {

                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void seasonInsert(SeasonInput vseason)
        {
            try
            {
                Seasons vsn = new Seasons();
                vsn.FromDate = Convert.ToDateTime(vseason.FromDate);
                vsn.ToDate = Convert.ToDateTime(vseason.ToDate);
                vsn.seasonInsert();
            }
            catch (Exception exp)
            {
            
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        public void seasonModify(SeasonInput vseason)
        {
            try
            {
                Seasons vsn = new Seasons();
                vsn.SeasonId = vseason.SeasonId;
                vsn.FromDate = Convert.ToDateTime(vseason.FromDate);
                vsn.ToDate = Convert.ToDateTime(vseason.ToDate);
                vsn.seasonModify();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        public void seasonDelete(SeasonInput vseason)
        {
            try
            {
                Seasons vsn = new Seasons();
                vsn.SeasonId = vseason.SeasonId;
                vsn.seasonDelete();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        /***************************Methods for Teams**************************/


        public List<TeamsInput> adminTeamSelect()
        {
            try
            {
                Teams t = new Teams();
                return t.adminTeamSelect();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<TeamsInput> userTeamSelect()
        {
            try
            {
                Teams t = new Teams();
                return t.userTeamSelect();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void teamInsert(TeamsInput teams)
        {
            try
            {
                Teams vteams = new Teams();
                vteams.TeamName = teams.TeamName;
                vteams.TeamLogo = teams.TeamLogo;
                vteams.teamInsert();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        public void teamModify(TeamsInput Vteams)
        {
            try
            {
                Teams vteams = new Teams();
                vteams.TeamId = Vteams.TeamId;
                vteams.TeamName = Vteams.TeamName;
                vteams.TeamLogo = Vteams.TeamLogo;
                vteams.teamModify();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public void teamDelete(TeamsInput teams)
        {
            try
            {
                Teams vteams = new Teams();
                vteams.TeamId = teams.TeamId;
                vteams.teamDelete();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
       
        /**************************************Methods For Prediction***************************************/
        public DataSet userPredictionSelect()
        {
            try
            {
                Predictions vpredict = new Predictions();
                return vpredict.userPredictionSelect();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }


        public void predictionInsert(string UserName, string MobileNumber, DataTable dtUserPredictions)
        {
            try
            {
                 Predictions vpredict = new Predictions();
                vpredict.predictionInsert(UserName, MobileNumber, dtUserPredictions);
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public DataSet adminPredictionResult()
        {
            try
            {
                Predictions vpredict = new Predictions();
                return vpredict.adminPredictionResult();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<PredictionInput> userPredictionDetails(string MobileNumber)
        {
            try
            {
                Predictions vpredict = new Predictions();
                return vpredict.userPredictionDetails(MobileNumber);
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }

        }

        public string getuserimage(string UserName , string MobileNumber)
        {
            try
            {
                Predictions vpredict = new Predictions();
                vpredict.UserName = UserName;
                vpredict.MobileNumber = MobileNumber;
                return vpredict.getUserImage(UserName, MobileNumber);
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
        /******************************Methods For Dropdown Binding*************************************/
        public List<SponsorInput> comboSponsorSelect()
        {
            try
            {
                Sponsors vsponsor = new Sponsors();
                return vsponsor.comboSponsorSelect();

            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }


        public List<TeamsInputCmb> comboAwayTeamselect()
        {
            try
            {
                Matches vmatch = new Matches();
                return vmatch.comboAwayTeamselect();
                //throw new NotImplementedException();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<TeamsInputCmb> comboHomeTeamselect()
        {
            try
            {
                Matches vmatch = new Matches();
                return vmatch.comboHomeTeamselect();
                //throw new NotImplementedException();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

     
        public void AdminInsert(UserInput user)
        {
            try
            {
                Users vus = new Users();
                vus.UserId = user.UserId;
                vus.UserName = user.UserName;
                vus.UserImage = user.UserImage;
                vus.MobileNumber = user.MobileNumber;
                vus.EmailId = user.EmailId;
                vus.City = user.City;
                vus.CreatedDate = user.CreatedDate;
                vus.adminInsert();
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public DataSet adminPlayerSearch(string searchPlayerName)
        {
            try
            {
                TeamPlayers vplayer = new TeamPlayers();
                vplayer.PlayerName = searchPlayerName;
                return vplayer.adminPlayerSearch(searchPlayerName);
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<GroundInputcmb> comboGroundNameSelect()
        {
            try
            {
                Matches vmatch = new Matches();
                return vmatch.comboGroundNameSelect();
                // throw new NotImplementedException();

            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<TeamsInput> comboWinnerTeamSelect()
        {
            try
            {
                Matches vmatch = new Matches();
                return vmatch.comboWinnerTeamSelect();
                // throw new NotImplementedException();

            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public List<TeamPlayerInput> ClientTeamSelect(string searchTeamName)
        {
            try
            {

                Teams vplayer = new Teams();
                vplayer.TeamName = searchTeamName;
                return vplayer.clientTeamSelect(searchTeamName);
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }

        public DataSet ClientTeamScheduleSelect(string searchTeamName)
        {
            try
            {

                Teams vplayer = new Teams();
                vplayer.TeamName = searchTeamName;
                return vplayer.ClientTeamScheduleSelect(searchTeamName);
            }
            catch (Exception exp)
            {
                MyFaultException theFault = new MyFaultException(exp.Message);
                throw new FaultException<MyFaultException>(theFault, new FaultReason(exp.Message), new FaultCode("Server"));
            }
        }
    }
}

