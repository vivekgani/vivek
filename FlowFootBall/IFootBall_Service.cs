﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using FlowFootBall.App_Code;
using System.ServiceModel.Web;

namespace FlowFootBall
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFootBall_Service" in both code and config file together.
    [ServiceContract]
    public interface IFootBall_Service
    {
        /**********************Methods of Teams************************/

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                          BodyStyle = WebMessageBodyStyle.Bare,
                                          UriTemplate = "adminTeamSelect/")]
        List<TeamsInput> adminTeamSelect();

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "userTeamSelect/")]
        List<TeamsInput> userTeamSelect();

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "teamInsert/")]
        void teamInsert(TeamsInput vteam);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "teamModify/")]
        void teamModify(TeamsInput teams);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "teamDelete/")]
        void teamDelete(TeamsInput teams);

        /*************************Methods of Sponsors**************************/

        /*************************Methods of AdminSponsorSelect**************************/


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "adminSponsorSelect/")]

        List<SponsorInput> adminSponsorSelect();

        /*************************Methods of UserSponsorSelect**************************/


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                          BodyStyle = WebMessageBodyStyle.Bare,
                          UriTemplate = "userSponsorSelect/")]

        List<SponsorInput> userSponsorSelect();

        /*************************Methods of SponsorInsert**************************/


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                             BodyStyle = WebMessageBodyStyle.Bare,
                             UriTemplate = "sponsorInsert/")]
        void sponsorInsert(SponsorInput vsponsor);


        /*************************Methods of SponsorModify**************************/


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                         BodyStyle = WebMessageBodyStyle.Bare,
                         UriTemplate = "sponsorModify/")]
        void sponsorModify(SponsorInput vsponsor);

        /*************************Methods of SponsorDelete**************************/


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                       BodyStyle = WebMessageBodyStyle.Bare,
                       UriTemplate = "sponsorDelete/")]
        void sponsorDelete(SponsorInput vsponsor);

        /*************************Methods of ComboxSponsorSelect**************************/


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                  BodyStyle = WebMessageBodyStyle.Bare,
                  UriTemplate = "comboSponsorSelect/")]
        List<SponsorInput> comboSponsorSelect();

        ///************************Methods of TeamPlayers***************************/

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "adminTeamPlayersSelect/")]
        List<TeamPlayerInput> adminTeamPlayersSelect();

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "userTeamPlayersSelect/")]
        List<TeamPlayerInput> userTeamPlayersSelect();

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "teamPlayerInsert/")]
        void teamPlayerInsert(TeamPlayerInput vtplayer);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "teamPlayerModify/")]
        void teamPlayerModify(TeamPlayerInput vtplayer);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "teamPlayerDelete/")]
        void teamPlayerDelete(TeamPlayerInput vtplayer);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "adminPlayerSearch/")]
        DataSet adminPlayerSearch(string searchPlayerName);


        ///***************************Methods of Grounds*******************************/


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "adminGroundSelect/")]
        [FaultContract(typeof(MyFaultException))]
        List<GroundInput> adminGroundSelect();


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "groundInsert/")]
        [FaultContract(typeof(MyFaultException))]
        void groundInsert(GroundInput vground);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "groundModify/")]
        [FaultContract(typeof(MyFaultException))]
        void groundModify(GroundInput vground);


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "groundDelete/")]
        [FaultContract(typeof(MyFaultException))]
        void groundDelete(GroundInput vground);

        ///******************************Methods of Seasons****************************/

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "adminSeasonSelect/")]
        [FaultContract(typeof(MyFaultException))]
        List<SeasonInput> adminSeasonSelect();

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                       BodyStyle = WebMessageBodyStyle.Bare,
                                       UriTemplate = "seasonInsert/")]
        [FaultContract(typeof(MyFaultException))]
        void seasonInsert(SeasonInput vseason);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                      BodyStyle = WebMessageBodyStyle.Bare,
                                      UriTemplate = "seasonModify/")]
        [FaultContract(typeof(MyFaultException))]
        void seasonModify(SeasonInput vseason);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                          BodyStyle = WebMessageBodyStyle.Bare,
                                          UriTemplate = "seasonDelete/")]
        [FaultContract(typeof(MyFaultException))]
        void seasonDelete(SeasonInput vseason);



        ///*****************************Methods of Matches********************************/

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "userMatchSelect/")]
        [FaultContract(typeof(MyFaultException))]
        DataSet userMatchSelect();

      
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "adminMatchSelect/")]
        [FaultContract(typeof(MyFaultException))]
        List<MatchInput> adminMatchSelect();


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "matchInsert/")]
        [FaultContract(typeof(MyFaultException))]
        void matchInsert(MatchInput vmatch);

        
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "matchmodify/")]
        [FaultContract(typeof(MyFaultException))]
        void matchModify(MatchInput vmatch);

       
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "matchDelete/")]
        [FaultContract(typeof(MyFaultException))]
        void matchDelete(MatchInput vmatch);


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "comboAwayTeamselect/")]
        [FaultContract(typeof(MyFaultException))]
        List<TeamsInputCmb> comboAwayTeamselect();

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "comboHomeTeamselect/")]
        [FaultContract(typeof(MyFaultException))]
        List<TeamsInputCmb> comboHomeTeamselect();

       
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "comboGroundNameSelect/")]
        [FaultContract(typeof(MyFaultException))]
        List<GroundInputcmb> comboGroundNameSelect();

       
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "comboWinnerTeamSelect/")]
        [FaultContract(typeof(MyFaultException))]
        List<TeamsInput> comboWinnerTeamSelect();

        /*****************************Methods of UserLogin***************************/

        //*****************************Method for UserInsert***************************/
     
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "userInsert/")]
        void userInsert(UserInput user);

        //*****************************Method for IsValidUser***************************/
     
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "isValidUser/")]
        bool isValidUser(UserInput user);

        //*****************************Method for IsvalidRole***************************/
    
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "isValidRole/")]
        bool isValidRole(UserInput user);

        //*****************************Method for AdminInsert***************************/
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "adminInsert/")]
        void adminInsert(UserInput user);


        ///*****************************Methods of Prediction***************************/
       
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "userPredictionSelect/")]
        DataSet userPredictionSelect();


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Wrapped,
                                 UriTemplate = "predictionInsert/")]
        void predictionInsert(string UserName, string MobileNumber, DataTable dtUserPredictions);

   
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "adminPredictionResult/")]
        DataSet adminPredictionResult();

       
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "userPredictionDetails/")]
        List<PredictionInput> userPredictionDetails(string MobileNumber);

       
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Wrapped,
                               UriTemplate = "getuserimage/")]
        string getuserimage(string UserName, string MobileNumber);

       
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "ClientTeamSelect/")]
        List<TeamPlayerInput> ClientTeamSelect(string searchTeamName);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "ClientTeamScheduleSelect/")]
        DataSet ClientTeamScheduleSelect(string searchTeamName);

    }

    /***************************Class for TeamPlayers**************************/

    [DataContract]
    public class TeamPlayerInput
    {
        private int VTeamPlayer_Id;
        private int VTeamId;
        private string VPlayerName;
        private string VPlayerImage;
        private string VTeamName;


        /**********************PROPERTY FOR TEAMPLAYER_ID**************************/
        [DataMember]
        public int TeamPlayer_Id
        {
            set { VTeamPlayer_Id = value; }
            get { return VTeamPlayer_Id; }
        }

        /**********************PROPERTY FOR TEAMID**************************/
        [DataMember]
        public int TeamId
        {
            set { VTeamId = value; }
            get { return VTeamId; }
        }

        /**********************PROPERTY FOR PLAYERNAME***********************/
        [DataMember]
        public string PlayerName
        {
            set { VPlayerName = value; }
            get { return VPlayerName; }
        }

        /**********************PROPERTY FOR PLAYERIMAGE***********************/
        [DataMember]
        public string PlayerImage
        {
            set { VPlayerImage = value; }
            get { return VPlayerImage; }
        }
        /**********************PROPERTY FOR TeamName***********************/
        [DataMember]
        public string TeamName
        {
            set { VTeamName = value; }
            get { return VTeamName; }
        }
    }

    /***********************************Class for Matches*****************************/

    [DataContract]
    public class MatchInput
    {
        private int VMatchId;
        private int VHomeTeam;
        private int VAwayTeam;
        private int VSeasonId;
        private int VWinnerTeamId;
        private int VGroundId;
        private string VMatchDate;


        /**********************PROPERTY FOR MATCHID**************************/
        [DataMember]
        public int MatchId
        {
            set { VMatchId = value; }
            get { return VMatchId; }
        }
        /**********************PROPERTY FOR HomeTeam **************************/
        [DataMember]
        public int HomeTeam
        {
            set { VHomeTeam = value; }
            get { return VHomeTeam; }
        }
        /**********************PROPERTY FOR AwayTeam **************************/
        [DataMember]
        public int AwayTeam
        {
            set { VAwayTeam = value; }
            get { return VAwayTeam; }
        }

        /**********************PROPERTY FOR SeasonId*****************/
        [DataMember]
        public int SeasonId
        {
            set { VSeasonId = value; }
            get { return VSeasonId; }
        }

        /**********************PROPERTY FOR WinnerTreamID*****************/

        [DataMember]
        public int WinnerTeamId
        {
            set { VWinnerTeamId = value; }
            get { return VWinnerTeamId; }
        }
        /**********************PROPERTY FOR GroundId*****************/

        [DataMember]
        public int GroundId
        {
            set { VGroundId = value; }
            get { return VGroundId; }
        }
        /**********************PROPERTY FOR MatchDate*****************/

        [DataMember]
        public string MatchDate
        {
            set { VMatchDate = value; }
            get { return VMatchDate; }
        }
        

    }

    /*****************************Class for Grounds***************************/

    [DataContract]
    public class GroundInput
    {
        private int VGroundId;
        private string VGroundName;
        private string VCity;

        [DataMember]
        public int GroundId
        {
            set { VGroundId = value; }
            get { return VGroundId; }
        }

        /**********************PROPERTY FOR GROUNDNAME************************/
        [DataMember]
        public string GroundName
        {
            set { VGroundName = value; }
            get { return VGroundName; }
        }

        /**********************PROPERTY FOR CITY******************************/
        [DataMember]
        public string City
        {
            set { VCity = value; }
            get { return VCity; }
        }
    }

    /******************************Class for MatchSchedule***************************/

    [DataContract]
    public class SeasonInput
    {
        private int VSeasonId;
        private string VFromDate;
        private string VToDate;

        /**********************PROPERTY FOR MatchSchedule_Id*******************/
        [DataMember]
        public int SeasonId
        {
            set { VSeasonId = value; }
            get { return VSeasonId; }
        }

        /**********************PROPERTY FOR FromDate**************************/
        [DataMember]
        public string FromDate
        {
            set { VFromDate = value; }
            get { return VFromDate; }
        }

        /**********************PROPERTY FOR FromDate**************************/

        [DataMember]
        public string ToDate
        {
            set { VToDate = value; }
            get { return VToDate; }
        }
    }

    /*******************************Class for sponsors*************************/

    [DataContract]
    public class SponsorInput
    {

        private int VSponsorId;
        private string VSponsorName;
        private string VSponsorImage;
        private int VSeasonId;


        /**********************PROPERTY FOR  SeasonSponsor_Id*****************/
        [DataMember]
        public int SponsorId
        {
            set { VSponsorId = value; }
            get { return VSponsorId; }
        }

        /**********************PROPERTY FOR  SponsorName*****************/
        [DataMember]
        public string SponsorName
        {
            set { VSponsorName = value; }
            get { return VSponsorName; }
        }

        /**********************PROPERTY FOR  SponsorImage*****************/
        [DataMember]
        public string SponsorImage
        {
            set { VSponsorImage = value; }
            get { return VSponsorImage; }
        }

        [DataMember]
        public int SeasonId
        {
            set { VSeasonId = value; }
            get { return VSeasonId; }
        }
    }
}

/***************************Class for User Login**************************/

[DataContract]
public class UserInput
{
    private int VUserId;
    private string VUserName;
    private string VUserImage;
    private string VMobileNumber;
    private string VEmailId;
    private string VCity;
    private DateTime VCreatedDate;
    private int VRoleId;
    private string VRoleName;


    /**********************PROPERTY FOR USERID**************************/
    [DataMember]
    public int UserId
    {
        set { VUserId = value; }
        get { return VUserId; }
    }

    /**********************PROPERTY FOR USERIMAGE************************/
    [DataMember]
    public string UserImage
    {
        set { VUserImage = value; }
        get { return VUserImage; }
    }

    /**********************PROPERTY FOR USERNAME************************/
    [DataMember]
    public string UserName
    {
        set { VUserName = value; }
        get { return VUserName; }
    }

    /**********************PROPERTY FOR PLAYERNAME**********************/
    [DataMember]
    public string MobileNumber
    {
        set { VMobileNumber = value; }
        get { return VMobileNumber; }
    }

    /**********************PROPERTY FOR EMAILID************************/
    [DataMember]
    public string EmailId
    {
        set { VEmailId = value; }
        get { return VEmailId; }
    }

    /**********************PROPERTY FOR CITY****************************/
    [DataMember]
    public string City
    {
        set { VCity = value; }
        get { return VCity; }
    }

    /**********************PROPERTY FOR REGISTEREDDATE***********************/
    [DataMember]
    public DateTime CreatedDate
    {
        set { VCreatedDate = value; }
        get { return VCreatedDate; }
    }
    /**********************PROPERTY FOR RoleId***********************/
    [DataMember]
    public int RoleId
    {
        set { VRoleId = value; }
        get { return VRoleId; }

    }
    /**********************PROPERTY FOR RoleName***********************/
    [DataMember]
    public string RoleName
    {
        set { VRoleName = value; }
        get { return VRoleName; }
    }

}



/***************************Class for Admin Login**************************/

[DataContract]
public class AdminInput
{
    private int VAdminId;
    private string VAdminName;
    private string VAdminPassWord;

    /**********************PROPERTY FOR AdminId**************************/
    [DataMember]
    public int AdminId
    {
        set { VAdminId = value; }
        get { return VAdminId; }
    }

    /**********************PROPERTY FOR AdminName**************************/
    [DataMember]
    public string AdminName
    {
        set { VAdminName = value; }
        get { return VAdminName; }
    }

    /**********************PROPERTY FOR AdminPassWord**************************/
    [DataMember]
    public string AdminPassWord
    {
        set { VAdminPassWord = value; }
        get { return VAdminPassWord; }

    }
}

/****************************Class for Teams***************************/

[DataContract]
public class TeamsInput
{

    private int VTeamId;
    private string VTeamName;
    private string VTeamLogo;

    /**********************PROPERTY FOR TEAMID**************************/
    [DataMember]
    public int TeamId
    {
        set { VTeamId = value; }
        get { return VTeamId; }
    }

    /**********************PROPERTY FOR TEAMLOGO***********************/
    [DataMember]
    public string TeamLogo
    {
        set { VTeamLogo = value; }
        get { return VTeamLogo; }
    }
    /**********************PROPERTY FOR TEAMNAME************************/
    [DataMember]
    public string TeamName
    {
        set { VTeamName = value; }
        get { return VTeamName; }
    }
}

/****************************Class for Predictions***************************/

[DataContract]
public class PredictionInput
{
    private int VPredictionId;
    private int VUserId;
    private int VMatchId;
    private int VWinnerTeamId;
    private int VHomeTeamId;
    private int VAwayTeamId;
    private DateTime VPredictedDate;
    private int VPredictedTeamId;
    private string VTeamName;
    private string VAwayTeamName;
    private string VEmailId;
    private string VUserName;
    /**********************PROPERTY FOR PredictionId******************************/
    [DataMember]
    public int PredictionId
    {
        set { VPredictionId = value; }
        get { return VPredictionId; }
    }

    /**********************PROPERTY FOR USERID******************************/
    [DataMember]
    public int UserId
    {
        set { VUserId = value; }
        get { return VUserId; }
    }


    /**********************PROPERTY FOR MATCHID******************************/
    [DataMember]
    public int MatchId
    {
        set { VMatchId = value; }
        get { return VMatchId; }
    }

    /**********************PROPERTY FOR PREDICTEDDATE***********************/
    [DataMember]
    public DateTime PredictedDate
    {
        set { VPredictedDate = value; }
        get { return VPredictedDate; }

    }
    /**********************PROPERTY FOR WinnerTeamId******************************/
    [DataMember]
    public int WinnerTeamId
    {
        set { VWinnerTeamId = value; }
        get { return VWinnerTeamId; }
    }
    /**********************PROPERTY FOR HomeTeamId******************************/
    [DataMember]
    public int HomeTeamId
    {
        set { VHomeTeamId = value; }
        get { return VHomeTeamId; }
    }
    /**********************PROPERTY FOR AwayTeamId******************************/
    [DataMember]
    public int AwayTeamId
    {
        set { VAwayTeamId = value; }
        get { return VAwayTeamId; }
    }
    /**********************PROPERTY FOR PredictedTeamId******************************/
    [DataMember]
    public int PredictedTeamId
    {
        set { VPredictedTeamId = value; }
        get { return VPredictedTeamId; }
    }
    /**********************PROPERTY FOR TeamName******************************/
    [DataMember]
    public string TeamName
    {
        set { VTeamName = value; }
        get { return VTeamName; }
    }
    /**********************PROPERTY FOR AwayTeamName******************************/
    [DataMember]
    public string AwayTeamName
    {
        set { VAwayTeamName = value; }
        get { return VAwayTeamName; }
    }

    /**********************PROPERTY FOR EmailId******************************/
    [DataMember]
    public string EmailId
    {
        set { VEmailId = value; }
        get { return VEmailId; }
    }

    /**********************PROPERTY FOR UserName******************************/
    [DataMember]
    public string UserName
    {
        set { VUserName = value; }
        get { return VUserName; }
    }

}
/****************************Class for FalutException Type***************************/


[DataContract]
public class MyFaultException
{
    private string _reason;

    public MyFaultException(string message)
    {
        _reason = message;
    }


    [DataMember]
    public string Reason
    {

        get { return _reason; }

        set { _reason = value; }

    }
}

/****************************Class for DropdownTeams***************************/

[DataContract]
public class TeamsInputCmb
{

    private int VId;
    private string VHomeTeamName;
    private string VAwayTeamName;


    /**********************PROPERTY FOR TEAMID**************************/
    [DataMember]
    public int Id
    {
        set { VId = value; }
        get { return VId; }
    }


    /**********************PROPERTY FOR TEAMNAME************************/
    [DataMember]
    public string HomeTeam
    {
        set { VHomeTeamName = value; }
        get { return VHomeTeamName; }
    }


    /**********************PROPERTY FOR TEAMNAME************************/
    [DataMember]
    public string AwayTeam
    {
        set { VAwayTeamName = value; }
        get { return VAwayTeamName; }
    }
}

/****************************Class for DropdownGrounds***************************/

[DataContract]
public class GroundInputcmb
{
    private int VId;
    private string VGroundName;

    /**********************PROPERTY FOR GroundId************************/
    [DataMember]
    public int Id
    {
        set { VId = value; }
        get { return VId; }
    }

    /**********************PROPERTY FOR GROUNDNAME************************/
    [DataMember]
    public string GroundName
    {
        set { VGroundName = value; }
        get { return VGroundName; }
    }

}

/****************************Class for Dropdown Roles***************************/

[DataContract]
public class RoleInputcmb
{
    private int VId;
    private string VRoleName;

    /**********************PROPERTY FOR RoleId************************/
    [DataMember]
    public int Id
    {
        set { VId = value; }
        get { return VId; }
    }

    /**********************PROPERTY FOR RoleName************************/
    [DataMember]
    public string RoleName
    {
        set { VRoleName = value; }
        get { return VRoleName; }
    }

}

[DataContract]
public class TeamPlayercmb
{
    private int VId;
    private string VTeamName;

    /**********************PROPERTY FOR RoleId************************/
    [DataMember]
    public int Id
    {
        set { VId = value; }
        get { return VId; }
    }

    /**********************PROPERTY FOR RoleName************************/
    [DataMember]
    public string TeamName
    {
        set { VTeamName = value; }
        get { return VTeamName; }
    }

}







