﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace FlowFootBall.App_Code
{
    public class Teams
    {
        FootBallEntities VFBE = new FootBallEntities();

        private int VTeamId;
        private string VTeamName;
        private string VTeamLogo;

        /**********************PROPERTY FOR TEAMID**************************/
        public int TeamId
        {
            set { VTeamId = value; }
            get { return VTeamId; }
        }

        /**********************PROPERTY FOR TEAMNAME************************/

        public string TeamName
        {
            set { VTeamName = value; }
            get { return VTeamName; }
        }

        /**********************PROPERTY FOR TEAMLOGO***********************/

        public string TeamLogo
        {
            set { VTeamLogo = value; }
            get { return VTeamLogo; }
        }

        /*******************METHOD FOR INSERTING A TEAM*********************/

        public void teamInsert()
        {
            try
            {
                bool VCheck_Name = (from vteam in VFBE.TEAMS where vteam.TeamName == TeamName select vteam).Count() > 0;
                if (VCheck_Name)
                {
                    TEAM VTeam = (from vteam in VFBE.TEAMS where vteam.TeamName == TeamName select vteam).Single();
                    VTeam.TeamStatus = "TRUE";
                    VFBE.SaveChanges();

                }
                else
                {
                    TEAM vteam = new TEAM();
                    vteam.TeamName = TeamName;
                    vteam.TeamLogo = TeamLogo;
                    vteam.TeamStatus = "TRUE";
                    VFBE.TEAMS.Add(vteam);
                    VFBE.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR Modifying A TEAM*********************/

        public void teamModify()
        {
            try
            {
                bool VCheck_Name = (from vteam in VFBE.TEAMS where vteam.TeamName == TeamName select vteam).Count() > 0;
                if (VCheck_Name)
                {
                    TEAM VTeam = (from vteam in VFBE.TEAMS where vteam.TeamName == TeamName select vteam).Single();
                    VTeam.TeamStatus = "TRUE";
                    VFBE.SaveChanges();

                }
                else
                {

                    var vteam = (from vtm in VFBE.TEAMS
                                 where vtm.TeamId == TeamId
                                 select vtm).Single();
                    TEAM vteam1 = vteam;

                    vteam1.TeamName = TeamName;
                    vteam1.TeamLogo = TeamLogo;
                    VFBE.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR Deleting A TEAM*********************/

        public void teamDelete()
        {
            try
            {
                TEAM vteam = new TEAM();
                var delteam = (from vtm in VFBE.TEAMS
                               where vtm.TeamId == TeamId
                               select vtm).Single();
                delteam.TeamStatus = "FALSE";
                VFBE.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR selecting A TEAM*********************/

        public List<TeamsInput> adminTeamSelect()
        {
            try
            {
                var select = from team in VFBE.TEAMS
                             where team.TeamStatus == "TRUE"
                             select new { team.TeamId, team.TeamLogo, team.TeamName };

                List<TeamsInput> VteamList = new List<TeamsInput>();
                foreach (var vsel in select)
                {
                    TeamsInput vteam = new TeamsInput();
                    vteam.TeamId = vsel.TeamId;
                    vteam.TeamLogo = vsel.TeamLogo;
                    vteam.TeamName = vsel.TeamName;
                    VteamList.Add(vteam);
                }
                return VteamList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR selecting A TEAM*********************/

        public List<TeamsInput> userTeamSelect()
        {
            try
            {

                var select = from data in VFBE.TEAMS where data.TeamStatus == "TRUE" select new { data.TeamId, data.TeamLogo, data.TeamName };


                List<TeamsInput> VteamList = new List<TeamsInput>();
                foreach (var vsel in select)
                {
                    TeamsInput vteam = new TeamsInput();
                    vteam.TeamId = vsel.TeamId;
                    vteam.TeamLogo = vsel.TeamLogo;
                    vteam.TeamName = vsel.TeamName;
                    VteamList.Add(vteam);
                }
                return VteamList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TeamPlayerInput> clientTeamSelect(string searchTeamName)
        {
            try
            {
                int teamid = (from data in VFBE.TEAMS where data.TeamName == searchTeamName select data.TeamId).SingleOrDefault();
                var players = from dt in VFBE.TEAM_PLAYERS where dt.TeamId == teamid select new { dt.PlayerName, dt.PlayerImage };

                List<TeamPlayerInput> VteamList = new List<TeamPlayerInput>();
                foreach (var vsel in players)
                {
                    TeamPlayerInput vteamplayer = new TeamPlayerInput();
                    vteamplayer.PlayerName = vsel.PlayerName;
                    vteamplayer.PlayerImage = vsel.PlayerImage;
                    VteamList.Add(vteamplayer);
                }
                return VteamList;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public DataSet ClientTeamScheduleSelect(string searchTeamName)
        {
            int teamid = (from data in VFBE.TEAMS where data.TeamName == searchTeamName select data.TeamId).SingleOrDefault();
            try
            {
                string web = ConfigurationManager.ConnectionStrings["config"].ConnectionString;
                SqlConnection vcon = new SqlConnection(web);
                SqlCommand vcmd = new SqlCommand();
                vcmd.Connection = vcon;
                vcmd.CommandText = "GetTeamSchedule";
                vcmd.CommandType = CommandType.StoredProcedure;
                vcmd.Parameters.AddWithValue("@HomeTeam", teamid);
                SqlDataAdapter vda = new SqlDataAdapter();
                DataSet vds = new DataSet();
                vda.SelectCommand = vcmd;
                vda.Fill(vds);
                return vds;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}