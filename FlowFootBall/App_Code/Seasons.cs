﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FlowFootBall.App_Code
{
    public class Seasons
    {
        FootBallEntities VFBE = new FootBallEntities();
        private int  VSeasonId;
        private DateTime VFromDate;
        private DateTime VToDate;
        private string VSeasonStatus;
       

        /**********************PROPERTY FOR MatchSchedule_Id*******************/

        public int SeasonId
        {
            set { VSeasonId = value; }
            get { return VSeasonId; }
        }
        
        /**********************PROPERTY FOR FromDate**************************/

        public DateTime FromDate
        {
            set { VFromDate = value; }
            get { return VFromDate; }
        }


        /**********************PROPERTY FOR ToDate**************************/

        public DateTime ToDate
        {
            set { VToDate = value; }
            get { return VToDate; }
        }


        /**********************PROPERTY FOR SeasonStatus**************************/
        public string SeasonStatus
        {
            set { VSeasonStatus = value; }
            get { return VSeasonStatus; }
        }


        /*********************Method for selecting AdminSeasonSelect*********************/

        public List<SeasonInput> adminSeasonSelect()
        {
            try
            {
                var select = (from vseason in VFBE.SEASONS
                              where vseason.SeasonStatus == "TRUE"
                              select vseason).ToList();

                List<SeasonInput> vseasonlist = new List<SeasonInput>();
                foreach (var vsel in select)
                {
                    SeasonInput vseason = new SeasonInput();
                    vseason.SeasonId = vsel.SeasonId;
                    vseason.FromDate = vsel.FromDate.ToString();
                    vseason.ToDate = vsel.ToDate.ToString();
                    vseasonlist.Add(vseason);
                }
               
                return vseasonlist;

            }
            catch(Exception Ex)
            {
                throw Ex;
            }


        }

        /********************** Method For AdminSeasonInsert*************************/
        public void seasonInsert()
        {
            try
            {
                bool VCheck_Season = (from Vseason in VFBE.SEASONS where Vseason.SeasonId == SeasonId select Vseason).Count() > 0;
                if (VCheck_Season)
                {
                    SEASON vsen = (from Vseason in VFBE.SEASONS where Vseason.SeasonId == SeasonId select Vseason).Single();
                    vsen.SeasonStatus = "TRUE";
                    VFBE.SaveChanges();
                }
                else
                {
                    SEASON vseason = new SEASON();
                    vseason.FromDate = FromDate;
                    vseason.ToDate = ToDate;
                    VFBE.SEASONS.Add(vseason);
                    VFBE.SaveChanges();
                }
            }

            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /********************** Method For AdminSeasonModify*************************/
        public void seasonModify()
        {
            try
            {
                    SEASON vseason = (from vmatch in VFBE.SEASONS where vmatch.SeasonId == SeasonId select vmatch).Single();
                    vseason.FromDate = FromDate;
                    vseason.ToDate = ToDate;
                    VFBE.SaveChanges();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /********************** Method For AdminSeasonDelete*************************/
        public void seasonDelete()
        {
            try
            {
                SEASON Vseason = (from match in VFBE.SEASONS where match.SeasonId == SeasonId select match).Single();
                Vseason.SeasonStatus = "FALSE";
                VFBE.SaveChanges();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}