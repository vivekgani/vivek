﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlowFootBall.App_Code
{
    public class Users
    {
        FootBallEntities VFBE = new FootBallEntities();
        private int VUserId;
        private string VUserName;
        private string VUserImage;
        private string VMobileNumber;
        private string VEmailId;
        private string VCity;
        private DateTime VCreatedDate;
        private int VRoleId;
        private string VRoleName;



        /**********************PROPERTY FOR USERID**************************/
        public int UserId
        {
            set { VUserId = value; }
            get { return VUserId; }
        }

        /**********************PROPERTY FOR USERNAME************************/

        public string UserName
        {
            set { VUserName = value; }
            get { return VUserName; }
        }

        /**********************PROPERTY FOR USERIMAGE************************/

        public string UserImage
        {
            set { VUserImage = value; }
            get { return VUserImage; }
        }
        /**********************PROPERTY FOR PLAYERNAME***********************/

        public string MobileNumber
        {
            set { VMobileNumber = value; }
            get { return VMobileNumber; }
        }

        /**********************PROPERTY FOR EMAILID************************/

        public string EmailId
        {
            set { VEmailId = value; }
            get { return VEmailId; }
        }

        /**********************PROPERTY FOR CITY****************************/

        public string City
        {
            set { VCity = value; }
            get { return VCity; }
        }

        /**********************PROPERTY FOR REGISTEREDDATE***********************/

        public DateTime CreatedDate
        {
            set { VCreatedDate = value; }
            get { return VCreatedDate; }
        }
        /**********************PROPERTY FOR RoleId***********************/
        public int RoleId
        {
            set { VRoleId = value; }
            get { return VRoleId; }

        }
        /**********************PROPERTY FOR RoleName***********************/
        public string RoleName
        {
            set { VRoleName = value; }
            get { return VRoleName; }
        }


        /*******************METHOD FOR INSERTING A USER*********************/

        public void userInsert()
        {
            try
            {
                USER vuser = new USER();
                vuser.UserName = UserName;
                vuser.UserImage = UserImage;
                vuser.MobileNumber = MobileNumber;
                vuser.City = City;
                vuser.EmailId = EmailId;
                vuser.CreatedDate = DateTime.Today;
                vuser.RoleId = 1;
                VFBE.USERS.Add(vuser);
                VFBE.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /***********************Method for validating an User***********************/

        public bool isValidUser()

        {
            try
            {
                USER vuser = new USER();
                var q = from p in VFBE.USERS where p.UserName == UserName && p.MobileNumber == MobileNumber select p;
                if (q.Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*************************method for Checking Role*************************/


        public bool isValidRole()

        {
            try
            {
                USER vuser = new USER();
                int role = Convert.ToInt32((from p in VFBE.USERS where p.UserName == UserName && p.MobileNumber == MobileNumber select p.RoleId).SingleOrDefault());
                if (role == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void adminInsert()
        {
            try
            {
                USER vuser = new USER();
                vuser.UserName = UserName;
                vuser.UserImage = UserImage;
                vuser.MobileNumber = MobileNumber;
                vuser.City = City;
                vuser.EmailId = EmailId;
                vuser.CreatedDate = DateTime.Today;
                vuser.RoleId = 2;
                VFBE.USERS.Add(vuser);
                VFBE.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}