﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace FlowFootBall.App_Code
{
    public class Matches
    {
        FootBallEntities VFBE = new FootBallEntities();
        private int VMatchId;
        private int VHomeTeam;
        private int VAwayTeam;
        private int VSeasonId;
        private int VWinnerTeamId;
        private int VGroundId;
        private DateTime VMatchDate;
        private string VGroundName;
        private string VTeamName;
        
        /**********************PROPERTY FOR MATCHID**************************/

        public int MatchId
        {
            set { VMatchId = value; }
            get { return VMatchId; }
        }
        /**********************PROPERTY FOR HomeTeam **************************/
        public int HomeTeam
        {
            set { VHomeTeam = value; }
            get { return VHomeTeam; }
        }
        /**********************PROPERTY FOR AwayTeam **************************/
        public int AwayTeam
        {
            set { VAwayTeam = value; }
            get { return VAwayTeam; }
        }

        /**********************PROPERTY FOR MatchSchedule_Id*****************/

        public int SeasonId
        {
            set { VSeasonId = value; }
            get { return VSeasonId; }
        }

       /**********************PROPERTY FOR WinnerTreamID*****************/

        public int WinnerTeamId
        {
            set { VWinnerTeamId = value; }
            get { return VWinnerTeamId; }
        }

        public int GroundId
        {
            set { VGroundId = value; }
            get { return VGroundId; }
        }

        public DateTime MatchDate
        {
            set { VMatchDate = value; }
            get { return VMatchDate; }
        }

        public string GroundName
        {
            set { VGroundName = value; }
            get { return VGroundName; }
        }
       
            public string TeamName
                {
                    set { VTeamName = value; }
                    get { return VTeamName; }
                }




        /********************** Method For Select*************************/
        public DataSet  userMatchSelect()
        {
            try
            {
                string web = ConfigurationManager.ConnectionStrings["config"].ConnectionString;
                SqlConnection vcon = new SqlConnection(web);
                SqlCommand vcmd = new SqlCommand();
                vcmd.Connection = vcon;
                vcmd.CommandText = "GetMatchSchedule";
                vcmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter vda = new SqlDataAdapter();
                DataSet vds = new DataSet();
                vda.SelectCommand = vcmd;
                vda.Fill(vds);
                return vds;
            }
            catch(Exception Vmselect)
            {
                throw Vmselect;
            }         
        }

        /********************** Method For inserting a match*************************/

        public void matchInsert()
        {
            try
            {
                MATCH vmatch = new MATCH();
               
                vmatch.SeasonId = SeasonId;
                vmatch.GroundId = GroundId;
                vmatch.MatchDate = MatchDate;
               
                vmatch.WinnerTeamId = WinnerTeamId;
                vmatch.HomeTeam = HomeTeam;
                vmatch.AwayTeam = AwayTeam;
                VFBE.MATCHES.Add(vmatch);
                VFBE.SaveChanges();
             }
            catch(Exception Vminsert)
            {
                throw Vminsert;
            }
        }

        /********************** Method For Modify*************************/

        public void matchModify()
        {
            try
            {


                //bool vmatch = (from vmat in VFBE.MATCHES where vmat.MatchId == MatchId select vmat).Count() > 0;
                //if (vmatch)
                //{
                //    MATCH vmatches = (from vmat in VFBE.MATCHES where vmat.MatchId == MatchId select vmat).Single();
                //    vmatches.MatchStatus = "TRUE";
                //}
                //else
                //{
                    var vmatchas = (from vmat in VFBE.MATCHES where vmat.MatchId == MatchId select vmat).Single();
                    
                    vmatchas.MatchId = MatchId;
                    vmatchas.SeasonId = SeasonId;
                    vmatchas.WinnerTeamId = WinnerTeamId;
                    vmatchas.GroundId = GroundId;
                    vmatchas.HomeTeam = HomeTeam;
                    vmatchas.AwayTeam = AwayTeam;
                    vmatchas.MatchDate = MatchDate;
                    VFBE.SaveChanges();
                //}
            }
            catch (Exception VMmodify)
            {
                throw VMmodify;
            }
        }

        /********************** Method For Delete*************************/

        public void matchDelete()
        {
            try
            {
                MATCH vmatch = (from vmat in VFBE.MATCHES where vmat.MatchId == MatchId select vmat).Single();
                vmatch.MatchStatus = "FALSE";
                VFBE.SaveChanges();
                
            }
            catch(Exception Vmdelete)
            {
                throw Vmdelete;
            }
        }

        /********************** Method For Select*************************/

        public List<MatchInput> adminMatchselect()
        {
            try
            {
                var select1 = (from vmatches in VFBE.MATCHES
                              join vteams in VFBE.TEAMS on vmatches.HomeTeam equals vteams.TeamId
                              join vgrnd in VFBE.GROUNDS on vmatches.GroundId equals vgrnd.GroundId
                              where vmatches.MatchStatus == "TRUE"
                              select new
                              {
                                  vmatches.MatchId,
                                  vteams.TeamName,
                                  awayTeam = from m1 in VFBE.MATCHES
                                             join t1 in VFBE.TEAMS on m1.AwayTeam equals t1.TeamId
                                             where m1.MatchStatus == "TRUE" & vmatches.HomeTeam == m1.HomeTeam
                                             select (t1.TeamName),
                                   vgrnd.GroundName,
                                   vmatches.SeasonId,
                                   vmatches.MatchDate,
                                   WinnerTeamName= (from m1 in VFBE.MATCHES
                                                 join t1 in VFBE.TEAMS on m1.HomeTeam equals t1.TeamId
                                                 join t2 in VFBE.TEAMS on m1.AwayTeam equals t2.TeamId
                                                 where m1.MatchStatus == "TRUE" & vmatches.HomeTeam == m1.HomeTeam &vmatches.AwayTeam==m1.AwayTeam
                                                 select (t1.TeamName)),
                                            
                              });

                List<MatchInput> VMatchlist = new List<MatchInput>();

                foreach (var vsel in select1)
                {
                    MatchInput vmatch = new MatchInput();
                    vmatch.MatchId = vsel.MatchId;
                    vmatch.HomeTeam = int.Parse(vsel.TeamName);
                    vmatch.AwayTeam = int.Parse(vsel.awayTeam.ToList().First());
                    vmatch.GroundId = int.Parse(vsel.GroundName);
                    vmatch.SeasonId = int.Parse(vsel.SeasonId.ToString());
                    vmatch.WinnerTeamId = int.Parse(vsel.WinnerTeamName.ToList().First());
                    vmatch.MatchDate = vsel.MatchDate.ToString();
                    VMatchlist.Add(vmatch);
                }

                return VMatchlist;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /********************** Method For Selecting HomeTeamNames From comboBox*************************/
        public List<TeamsInputCmb> comboHomeTeamselect()
        {
            try
            {
                var VHomeTeamName = (from vteam in VFBE.TEAMS
                                     select new TeamsInputCmb
                                     {
                                         Id = vteam.TeamId,
                                         HomeTeam = vteam.TeamName

                                     }).ToList();

                return VHomeTeamName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /********************** Method For Selecting AwayTeamNames From comboBox*************************/
        public List<TeamsInputCmb> comboAwayTeamselect()
        {
            try
            {
                var VAwayTeamName = (from vteam in VFBE.TEAMS
                                     select new TeamsInputCmb
                                     {
                                         Id = vteam.TeamId,
                                         AwayTeam = vteam.TeamName

                                     }).ToList();

                return VAwayTeamName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /********************** Method For getting the GroundId based on Groundnames*************************/
        public List<GroundInputcmb> comboGroundNameSelect()
        {
            try
            {
                var VGroundName = (from vground in VFBE.GROUNDS
                                  
                                   select new GroundInputcmb

                                   {
                                       Id = vground.GroundId,
                                       GroundName = vground.GroundName
                                   }).ToList();


                return VGroundName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TeamsInput> comboWinnerTeamSelect()
        {
            try
            {
                var VWinnerTeamName = (from vteams in VFBE.TEAMS
                                    select new TeamsInput
                                    {     TeamId=vteams.TeamId,
                                          TeamName=vteams.TeamName
                                    }).ToList();

                return VWinnerTeamName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

///********************** Method For getting the HomeTeamid based on HomeTeamnames*************************/
//public  int ComboselectedHomeTeamName(string TeamName)
//{
//    try
//    {
//        var HomeTeamId = (from vteam in VFBE.TEAMS where vteam.TeamName == TeamName select vteam.TeamId).Single();

//        return HomeTeamId;
//    }
//    catch (Exception ex)
//    {
//        throw ex;
//    }
//}
