﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace FlowFootBall.App_Code
{
    public class Predictions
    {
        FootBallEntities VFBE = new FootBallEntities();
        private int VPredictionId;
        private int VUserId;
        private int VMatchId;
        private int VWinnerTeamId;
        private int VHomeTeamId;
        private int VAwayTeamId;
        private string VUserName;
        private DateTime VPredictedDate;
        private string VMobileNumber;

        /**********************PROPERTY FOR PREDICTIONID******************************/

        public int PredictionId
        {
            set { VPredictionId = value; }
            get { return VPredictionId; }
        }

        /**********************PROPERTY FOR USERID******************************/

        public int UserId
        {
            set { VUserId = value; }
            get { return VUserId; }
        }


        /**********************PROPERTY FOR MATCHID******************************/

        public int MatchId
        {
            set { VMatchId = value; }
            get { return VMatchId; }
        }

        /**********************PROPERTY FOR PREDICTEDDATE***********************/

        public DateTime PredictedDate
        {
            set { VPredictedDate = value; }
            get { return VPredictedDate; }
        }
        /**********************PROPERTY FOR WINNERTEAMID***********************/
        public int WinnerTeamId
        {
            set { VWinnerTeamId = value; }
            get { return VWinnerTeamId; }
        }
        /**********************PROPERTY FOR HOMETEAMID***********************/
        public int HomeTeamId
        {
            set { VHomeTeamId = value; }
            get { return VHomeTeamId; }
        }
        /**********************PROPERTY FOR AwayTEAMID***********************/
        public int AwayTeamId
        {
            set { VAwayTeamId = value; }
            get { return VAwayTeamId; }
        }
        /**********************PROPERTY FOR USERNAME***********************/

        public string UserName
        {
            set { VUserName = value; }
            get { return VUserName; }
        }

        /**********************PROPERTY FOR MobileNumber***********************/
        public string MobileNumber
        {
            set { VMobileNumber = value; }
            get { return VMobileNumber; }
        }

        public DataSet userPredictionSelect()
        {
            try
            {
                string web = ConfigurationManager.ConnectionStrings["config"].ConnectionString;
                SqlConnection vcon = new SqlConnection(web);
                SqlCommand vcmd = new SqlCommand();
                vcmd.Connection = vcon;
                vcmd.CommandText = "UserPrediction";
                vcmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter vda = new SqlDataAdapter();
                DataSet vds = new DataSet();
                vda.SelectCommand = vcmd;
                vda.Fill(vds);
                return vds;
            }
            catch (Exception VPselect)
            {
                throw VPselect;
            }

        }


        public void predictionInsert(string UserName, string MobileNumber, DataTable dtUserPredictions)
        {
            PREDICTION vpredict = new PREDICTION();
            int VUserId = (from vuser in VFBE.USERS where vuser.UserName == UserName & vuser.MobileNumber == MobileNumber select vuser.UserId).Single();

            string web = ConfigurationManager.ConnectionStrings["config"].ConnectionString;
            SqlConnection vcon = new SqlConnection(web);
            SqlCommand vcmd = new SqlCommand();
            vcmd.Connection = vcon;
            vcmd.CommandText = "InsertUserPrediction";
            vcmd.CommandType = CommandType.StoredProcedure;
            vcmd.Parameters.AddWithValue("@UserId", VUserId);
            vcmd.Parameters.AddWithValue("@MobileNumber", MobileNumber);
            vcmd.Parameters.AddWithValue("@UserPredictionType", dtUserPredictions);
            vcon.Open();
            vcmd.ExecuteNonQuery();
            vcon.Close();

        }

        public DataSet adminPredictionResult()
        {
            try
            {
                PREDICTION vpredict = new PREDICTION();
                string web = ConfigurationManager.ConnectionStrings["config"].ConnectionString;
                SqlConnection vcon = new SqlConnection(web);
                SqlCommand vcmd = new SqlCommand();
                vcmd.Connection = vcon;
                vcmd.CommandText = "GetPredictionResult";
                vcmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter vda = new SqlDataAdapter();
                DataSet vds = new DataSet();
                vda.SelectCommand = vcmd;
                vda.Fill(vds);
                return vds;
            }
            catch (Exception VPselect)
            {
                throw VPselect;
            }
        }

        public List<PredictionInput> userPredictionDetails(string MobileNumber)
        {
            try
            {
                int UserId = (from vuser in VFBE.USERS where vuser.MobileNumber == MobileNumber select vuser.UserId).Single();
                string VMobileNumber = (from vuser in VFBE.USERS where vuser.MobileNumber == MobileNumber select vuser.MobileNumber).Single();
                var Prediction_Details = from vuser in VFBE.USERS
                                         join vpredict in VFBE.PREDICTIONS on vuser.UserId equals vpredict.UserId
                                         join vteams in VFBE.TEAMS on vpredict.HomeTeamId equals vteams.TeamId
                                         where vpredict.UserId == UserId
                                         select new
                                         {
                                             vpredict.MatchId,
                                             vpredict.PredictedTeamId,
                                             vpredict.UserId,
                                             vteams.TeamName,
                                             AwayTeamName = from Predictions in VFBE.PREDICTIONS
                                                            join teams in VFBE.TEAMS on Predictions.AwayTeamId equals teams.TeamId
                                                            where Predictions.UserId == UserId & Predictions.HomeTeamId == vpredict.HomeTeamId
                                                            select (teams.TeamName),

                                             vuser.EmailId,
                                             vuser.UserName


                                         };
                List<PredictionInput> Vprediction = new List<PredictionInput>();
                foreach (var vsel in Prediction_Details)
                {
                    PredictionInput vpredict = new PredictionInput();
                    vpredict.MatchId = Convert.ToInt32(vsel.MatchId);
                    vpredict.PredictedTeamId = Convert.ToInt32(vsel.PredictedTeamId);
                    vpredict.UserId = vsel.UserId;
                    vpredict.TeamName = vsel.TeamName;
                    vpredict.AwayTeamName = Convert.ToString(vsel.AwayTeamName);
                    vpredict.EmailId = vsel.EmailId;
                    vpredict.UserName = vsel.UserName;
                    Vprediction.Add(vpredict);
                }
                return Vprediction;


            }
            catch (Exception VPselect)
            {
                throw VPselect;
            }
        }

        public string getUserImage(string UserName, string MobileNumber)
        {
            try
            {
                USER vuser = new USER();

                string vuimg = (from user in VFBE.USERS where user.UserName == UserName & user.MobileNumber == MobileNumber select user.UserImage).Single();
                return vuimg;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}