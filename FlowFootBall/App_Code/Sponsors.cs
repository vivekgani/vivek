﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace FlowFootBall.App_Code
{
    public class Sponsors
    {
        FootBallEntities VFBE = new FootBallEntities();

        private int VSponsorId;
        private string VSponsorName;
        private string VSponsorImage;
        private int VSeasonId;

        /**********************PROPERTY FOR  SeasonSponsor_Id*****************/

        public int SponsorId
        {
            set { VSponsorId = value; }
            get { return VSponsorId; }
        }

        /**********************PROPERTY FOR  SponsorName*****************/

        public string SponsorName
        {
            set { VSponsorName = value; }
            get { return VSponsorName; }
        }

        /**********************PROPERTY FOR  SponsorImage*****************/

        public string SponsorImage
        {
            set { VSponsorImage = value; }
            get { return VSponsorImage; }
        }

        /**********************PROPERTY FOR MatchSchedule_Id*****************/

        public int SeasonId
        {
            set { VSeasonId = value; }
            get { return VSeasonId; }
        }

        /*******************METHOD FOR Selecting A SPONSOR*********************/

        public List<SponsorInput> adminSponsorSelect()
        {
            try
            {
                var select = from vsponsor in VFBE.SPONSORS
                             where vsponsor.SponsorStatus == "TRUE"
                             select new
                             {
                                 vsponsor.SponsorId,
                                 vsponsor.SponsorName,
                                 vsponsor.SponsorImage,
                                 vsponsor.SeasonId
                             };

                List<SponsorInput> vsponsorList = new List<SponsorInput>();

                foreach (var vsel in select)
                {
                    SponsorInput vspon = new SponsorInput();
                    vspon.SeasonId = int.Parse(vsel.SeasonId.ToString());
                    vspon.SponsorId = vsel.SponsorId;
                    vspon.SponsorName = vsel.SponsorName;
                    vspon.SponsorImage = vsel.SponsorImage;
                    vsponsorList.Add(vspon);

                }
                return vsponsorList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /*************************Method for select***************************/

        public List<SponsorInput> userSponsorSelect()
        {
            try
            {
                var select = from vgrnd in VFBE.SPONSORS
                             where vgrnd.SponsorStatus == "TRUE"
                             select new
                             {
                                 vgrnd.SponsorImage,

                             };

                List<SponsorInput> vsponsorList = new List<SponsorInput>();

                foreach (var vsel in select)
                {
                    SponsorInput vspon = new SponsorInput();
                    vspon.SponsorImage = vsel.SponsorImage;
                    vsponsorList.Add(vspon);
                }
                return vsponsorList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /*******************METHOD FOR INSERTING A SPONSOR*********************/

        public void sponsorInsert()
        {
            try
            {
                bool VCheck_Name = (from Vsponsor in VFBE.SPONSORS where Vsponsor.SponsorName == SponsorName select Vsponsor).Count() > 0;
                if (VCheck_Name)
                {
                    SPONSOR vsponsor = (from Vsponsor in VFBE.SPONSORS where Vsponsor.SponsorName == SponsorName select Vsponsor).Single();
                    vsponsor.SponsorStatus = "TRUE";
                    VFBE.SaveChanges();
                }
                else
                {

                    SPONSOR vsponsor = new SPONSOR();
                    vsponsor.SponsorName = SponsorName;
                    vsponsor.SponsorImage = SponsorImage;
                    vsponsor.SeasonId = SeasonId;
                    VFBE.SPONSORS.Add(vsponsor);
                    VFBE.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR Modifying A SPONSOR*********************/

        public void sponsorModify()
        {
            try
            {
                bool VCheck_Name = (from Vsponsor in VFBE.SPONSORS where Vsponsor.SponsorName == SponsorName select Vsponsor).Count() > 0;
                if (VCheck_Name)
                {
                    SPONSOR vsponsor = (from Vsponsor in VFBE.SPONSORS where Vsponsor.SponsorName == SponsorName select Vsponsor).Single();
                    vsponsor.SponsorStatus = "TRUE";
                    VFBE.SaveChanges();
                }
                else
                {
                    SPONSOR vsponsor = new SPONSOR();
                    var modsponsor = (from vsp in VFBE.SPONSORS
                                      where vsp.SponsorId == SponsorId
                                      select vsp).Single();
                    modsponsor.SponsorName = SponsorName;
                    modsponsor.SponsorImage = SponsorImage;
                    modsponsor.SeasonId = SeasonId;
                    VFBE.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR Deleting A SPONSOR*********************/

        public void sponsorDelete()
        {
            try
            {
                SPONSOR vsponsor = new SPONSOR();
                var delsponsor = (from vsp in VFBE.SPONSORS
                                  where vsp.SponsorId == SponsorId
                                  select vsp).Single();
                delsponsor.SponsorStatus = "FALSE";
                VFBE.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /**********************************Sponsor DropDown *****************************************/
        public List<SponsorInput> comboSponsorSelect()
        {
            try
            {
                var VSponsorName = (from vsponsor in VFBE.SPONSORS

                                    select new SponsorInput
                                    {
                                        SponsorId = vsponsor.SponsorId,
                                        SponsorName = vsponsor.SponsorName

                                    }).ToList();

                return VSponsorName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}