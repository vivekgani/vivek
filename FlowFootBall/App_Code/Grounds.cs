﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FlowFootBall.App_Code
{
    public class Grounds
    {
        FootBallEntities VFBE = new FootBallEntities();
        private int VGroundId;
        private string VGroundName;
        private string VCity;

        /**********************PROPERTY FOR GROUNDID**************************/
        public int GroundId
        {
            set { VGroundId = value; }
            get { return VGroundId; }
        }

        /**********************PROPERTY FOR GROUNDNAME************************/

        public string GroundName
        {
            set { VGroundName = value; }
            get { return VGroundName; }
        }

        /**********************PROPERTY FOR CITY******************************/

        public string City
        {
            set { VCity = value; }
            get { return VCity; }
        }

        /********************** Method For Insert*************************/
        public void groundInsert()
        {
            try
            {
                bool VCheck_Ground_Name = (from vground in VFBE.GROUNDS where vground.GroundName == GroundName select vground).Count() > 0;
                if (VCheck_Ground_Name)
                {
                    GROUND vgrnd = (from vground in VFBE.GROUNDS where vground.GroundName == GroundName select vground).Single();
                    vgrnd.GroundStatus = "TRUE";
                    VFBE.SaveChanges();
                }
                else
                {
                    GROUND vground = new GROUND();
                    vground.GroundName = GroundName;
                    vground.City = City;
                    vground.GroundStatus = "TRUE";
                    VFBE.GROUNDS.Add(vground);
                    VFBE.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /********************** Method For Modify*************************/

        public void groundModify()
        {
            try
            {

                //bool VCheck_Ground_Name = (from vground in VFBE.GROUNDS where vground.GroundName == GroundName select vground).Count() > 0;
                //if (VCheck_Ground_Name)
                //{
                //    GROUND vgrnd = (from vground in VFBE.GROUNDS where vground.GroundName == GroundName select vground).Single();
                //    vgrnd.GroundStatus = "TRUE";
                //    VFBE.SaveChanges();
                //}
                //else
                //{
                var Update_Ground = (from vground in VFBE.GROUNDS where vground.GroundId == GroundId select vground).Single();
                Update_Ground.GroundName = GroundName;
                Update_Ground.City = City;
                VFBE.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /********************** Method For Delete*************************/

        public void groundDelete()
        {
            try
            {
                var Delete_GroundName = from vground in VFBE.GROUNDS where vground.GroundId == GroundId select vground;
                GROUND vgrnd = Delete_GroundName.Single();
                vgrnd.GroundStatus = "FALSE";
                VFBE.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /********************method for selecting ground details***********************/

        public List<GroundInput>  adminGroundSelect()
        {
            try
            {
                var select = from data in VFBE.GROUNDS where data.GroundStatus == "TRUE" select new
                { data.GroundId, data.GroundName,data.City };

                List<GroundInput> Vgroundlist = new List<GroundInput>();
                foreach (var vsel in select)
                {
                    GroundInput vground = new GroundInput();
                    vground.GroundId = vsel.GroundId;
                    vground.GroundName = vsel.GroundName;
                    vground.City = vsel.City;
                    Vgroundlist.Add(vground);
                }
                return Vgroundlist;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}