﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Newtonsoft.Json;
namespace FlowFootBall.App_Code
{
    public class TeamPlayers
    {
        FootBallEntities VFBE = new FootBallEntities();
        private int VTeamPlayer_Id;
        private int VTeamId;
        private string VPlayerName;
        private string VPlayerImage;
        private string VTeamName;

        /**********************PROPERTY FOR TEAMPLAYER_ID**************************/
        public int TeamPlayer_Id
        {
            set { VTeamPlayer_Id = value; }
            get { return VTeamPlayer_Id; }
        }

        /**********************PROPERTY FOR TEAMID**************************/

        public int TeamId
        {
            set { VTeamId = value; }
            get { return VTeamId; }
        }

        /**********************PROPERTY FOR PLAYERNAME***********************/

        public string PlayerName
        {
            set { VPlayerName = value; }
            get { return VPlayerName; }
        }

        /**********************PROPERTY FOR PLAYERIMAGE***********************/

        public string PlayerImage
        {
            set { VPlayerImage = value; }
            get { return VPlayerImage; }
        }

        public string TeamName
        {
            set { VTeamName = value; }
            get { return VTeamName; }
        }

        /*************************Method for selecting teamplayers***********************/

        public List<TeamPlayerInput> adminTeamPlayersSelect()
        {
            try
            {
                var select = from data in VFBE.TEAM_PLAYERS
                             join vteam in VFBE.TEAMS on data.TeamId equals vteam.TeamId
                             where data.PlayerStatus == "TRUE"
                             select new { data.TeamPlayerId, vteam.TeamName, data.PlayerName, data.PlayerImage };
                List<TeamPlayerInput> VteamList = new List<TeamPlayerInput>();
                foreach (var vsel in select)
                {
                    TeamPlayerInput vteamplayer = new TeamPlayerInput();
                    vteamplayer.TeamPlayer_Id = vsel.TeamPlayerId;
                    vteamplayer.TeamName = vsel.TeamName;
                    vteamplayer.PlayerName = vsel.PlayerName;
                    vteamplayer.PlayerImage = vsel.PlayerImage;
                    VteamList.Add(vteamplayer);
                }
                return VteamList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*************************Method for selecting teamplayers***********************/

        public List<TeamPlayerInput> userTeamPlayersSelect()
        {
            try
            {
                var select = from data in VFBE.TEAM_PLAYERS where data.PlayerStatus == "TRUE" select new { data.TeamPlayerId, data.TeamId, data.PlayerName, data.PlayerImage };
                List<TeamPlayerInput> VteamList = new List<TeamPlayerInput>();
                foreach (var vsel in select)
                {
                    TeamPlayerInput vteamplayer = new TeamPlayerInput();
                    vteamplayer.TeamPlayer_Id = vsel.TeamPlayerId;
                    vteamplayer.TeamId = int.Parse((vsel.TeamId).ToString());
                    vteamplayer.PlayerName = vsel.PlayerName;
                    vteamplayer.PlayerImage = vsel.PlayerImage;
                    VteamList.Add(vteamplayer);
                }
                return VteamList;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******************METHOD FOR INSERTING A TEAMPLAYER*********************/

        public void teamPlayerInsert()
        {
            try
            {
                bool VCheck_Name = (from vteamplayer in VFBE.TEAM_PLAYERS where vteamplayer.PlayerName == PlayerName select vteamplayer).Count() > 0;
            if (VCheck_Name)
            {
                TEAM_PLAYERS VTeamplayer = (from vteamplayer in VFBE.TEAM_PLAYERS where vteamplayer.PlayerName == PlayerName select vteamplayer).Single();
                VTeamplayer.PlayerStatus = "TRUE";
                VFBE.SaveChanges();

            }
            else
            {
                //int vteamid = (from vteam in VFBE.TEAMS where vteam.TeamName == TeamName select vteam.TeamId).Single();
                TEAM_PLAYERS vteamplayer = new TEAM_PLAYERS();
                vteamplayer.TeamId = TeamId;
                vteamplayer.PlayerName = PlayerName;
                vteamplayer.PlayerImage = PlayerImage;
                vteamplayer.PlayerStatus = "TRUE";
                VFBE.TEAM_PLAYERS.Add(vteamplayer);
                VFBE.SaveChanges();
            }
        }
            catch (Exception ex)
            {
               throw ex;
            }
}

        /*******************METHOD FOR Modifying A TEAMPLAYER*********************/

        public void teamPlayerModify()
        {
            try
            {
                bool VCheck_Name = (from vteamplayer in VFBE.TEAM_PLAYERS where vteamplayer.PlayerName == PlayerName select vteamplayer).Count() > 0;
            if (VCheck_Name)
            {
                TEAM_PLAYERS VTeamplayer = (from vteamplayer in VFBE.TEAM_PLAYERS where vteamplayer.PlayerName == PlayerName select vteamplayer).Single();
                VTeamplayer.PlayerStatus = "TRUE";
                VFBE.SaveChanges();

            }
            else
            {
                TEAM_PLAYERS vteamplayer = new TEAM_PLAYERS();
                var modteamplayer = (from vtm in VFBE.TEAM_PLAYERS
                                     where vtm.TeamPlayerId == TeamPlayer_Id
                                     select vtm).Single();

                modteamplayer.TeamId = TeamId;
                modteamplayer.PlayerName = PlayerName;
                modteamplayer.PlayerImage = PlayerImage;
                VFBE.SaveChanges();
            }
        }
            catch (Exception ex)
            {
                throw ex;
            }
}

        /*******************METHOD FOR Deleting A TEAMPLAYER*********************/

        public void teamPlayerDelete()
        {
            try
            {
                TEAM_PLAYERS vteamplayer = new TEAM_PLAYERS();
                var delteamplayer = (from vtm in VFBE.TEAM_PLAYERS
                                     where vtm.TeamPlayerId == TeamPlayer_Id
                                     select vtm).Single();
                delteamplayer.PlayerStatus = "FALSE";
                VFBE.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /***********************************Method for seraching the Team Players*************************************/
        public DataSet adminPlayerSearch(string searchPlayerName)
        {
            string web = ConfigurationManager.ConnectionStrings["config"].ConnectionString;
            SqlConnection vcon = new SqlConnection(web);
            SqlCommand vcmd = new SqlCommand();
            vcmd.Connection = vcon;
            vcmd.CommandText = "playersearch";
            vcmd.CommandType = CommandType.StoredProcedure;
            vcmd.Parameters.AddWithValue("@PlayerName", searchPlayerName);
            SqlDataAdapter vda = new SqlDataAdapter();
            DataSet vds = new DataSet();
            vda.SelectCommand = vcmd;
            vda.Fill(vds);            
            return vds;

        }
    }
}